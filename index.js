const fs = require("fs");
const readline = require("readline");
const os = require("os");
const http = require("http");

const HOSTNAME = "127.0.0.1";
const PORT = 3000;
const FILENAME = "package.json";

/**
 * Console prints the file.
 * @param {string} filename - name of file that will be printed
 */
const readPackageJsonToConsole = async (filname) => {
  try {
    const data = await fs.promises.readFile(filname, "utf8");
    console.log(data);
  } catch (error) {
    console.log(error);
  }
};

/**
 * Prints information about the OS
 */
const printOSInformation = () => {
  console.log(
    `Getting OS info...
    SYSTEM MEMORY: ${(os.totalmem() / 1024 / 1024 / 1024).toFixed(2)}
    FREE MEMORY: ${(os.freemem() / 1024 / 1024 / 1024).toFixed(2)}
    CPU CORES: ${os.cpus().length}
    ARCH: ${os.arch()}
    PLATFORM: ${os.platform()}
    RELEASE: ${os.release()}
    USER: ${os.userInfo().username}`
  );
};

/**
 * start http server that is listening to port 3000
 */
const startServer = () => {
  const server = http.createServer((req, res) => {
    res.statusCode = 200;
    res.setHeader("Content-Type", "text/plain");
    res.end("Hello World");
  });

  server.listen(PORT, HOSTNAME, () => {
    console.log(`Server running at http://${HOSTNAME}:${PORT}/`);
  });
};

/**
 * Lets the user choose between 3 options.
 */
const startUserInput = () => {
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });

  rl.question(
    "Choose an option: \n 1. Read package.json \n 2. Display OS info \n 3. Start HTTP Server \n",
    (answer) => {
      switch (answer) {
        case "1":
          readPackageJsonToConsole(FILENAME);
          break;
        case "2":
          printOSInformation();
          break;
        case "3":
          startServer();
          break;
        default:
          console.log("invalid input");
      }
      rl.close();
    }
  );
};

startUserInput();
